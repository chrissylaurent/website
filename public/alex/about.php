<!DOCTYPE html>
<html>

<!-- The Info Page -->

<head>
<!-- Link Required Stylesheets and Fonts -->

<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Josefin+Slab">
<link href='http://fonts.googleapis.com/css?family=Lato:400,900' rel='stylesheet' type='text/css'>
<link href="../css/homestyle.css" type="text/css" rel="stylesheet">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
   $("#menu").hide();
      $("#menuicon").click(function(){
        $("#menu").toggle("slow");
    });
});
</script>
</head>
<body>

<!--php must be inside the body of the text-->

<h1 id="head"><span class="bold">HORLOCK'S</span>|InfoPage </h1>

<div id="viewcount">
<?php include 'php/infocounter.php';?>
</div>

<ul id="menu">
	<li> <a href="http://www.horlockspage.com">Home</a></li>
	<li> <a href="http://www.horlockspage.com/about.php">Overview</a>
	<li> <a href="http://www.horlockspage.com/projects.php">Projects</a></li> 
	<li> <a href="http://www.horlockspage.com/sport.php">Sport</a><li>
	<li> <a href="http://www.horlockspage.com/contact.php">Contact</a><li>
	<li> <a href="http://www.horlockspage.com/x.php">Private</a><li>
</ul>
<div id="bg">
	<img src="../images/background2.jpg" alt="">
</div>
<div id="menuicon">
	 <img src="../images/menu_icon.png" alt ="" height="50" width="50">
</div>

<a id="facebookicon" href="http://www.facebook.com/alex.horlock.5">
	<img src="../images/facebook_icon.png" alt="" height="42" width="42">
</a>

<a id="linkedinicon" href="http://www.linkedin.com/pub/alex-horlock/6a/92/52">
	<img src="../images/linkedin_icon.png" alt="" height="60" width="60" >
</a>

<a id="twittericon" href="http://www.twitter.com/AlexHorlock1">
	<img src="../images/twitter_icon.png" alt="" height="50" width="50">
</a>

</body>
<!-- Notes
The Z-index controls which elements appear on the top layer.
-->
</html>
